#include "D:\eigen\eigen-3.4.0\Eigen\Dense"
#include <iostream>
#include <cmath>
#include <vector>
#include "D:\eigen\eigen-3.4.0\Eigen\QR"
#include <iterator>
#include <random>

using namespace std;
using namespace Eigen;
void polyfit(const vector<int>& x_,const vector<double>& y_, vector<double>& coeff,int order){
	// Create Matrix Placeholder of size n x k, n= number of datapoints, k = order of polynomial, for exame k = 3 for cubic polynomial
	MatrixXd T(x_.size(), order + 1);
	VectorXd V = VectorXd::Map(&y_.front(), y_.size());
	VectorXd result;

	// check to make sure inputs are correct
	assert(x_.size() == y_.size());
	assert(x_.size() >= order + 1);
	// Populate the matrix
	for (size_t i = 0; i < x_.size(); ++i)
	{
		for (size_t j = 0; j < order+1; ++j)
		{
			T(i, j) = pow(x_[i], j);
		}
	}
	//cout << T << std::endl;

	// Solve for linear least square fit
	//result = T.householderQr().solve(V);
	result = T.bdcSvd(ComputeThinU | ComputeThinV).solve(V);
	//A.colPivHouseholderQr().solve(b)
	//A.bdcSvd(ComputeThinU | ComputeThinV).solve(b)
	coeff.resize(order + 1);
	for (int k = 0; k < order + 1; k++)
	{
		coeff[k] = result[k];
	}

}

int main()
{
	//x coordinates (in mm)
	vector<int> x_coord = {-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10};
	//corresponding y coordinates 
	vector<double> y_coord(21, 0);
	for (int i = 0; i < 21; i++) {
		y_coord[i] = .005 * pow(x_coord[i],3) + .05 * pow(x_coord[i],2) + 7 * x_coord[i] + 4;
	}
	cout << "Actual y_coordinates: "<<endl;
	for (int i = 0; i < y_coord.size(); i++) {
		cout << y_coord[i] << " ";
	}
	cout << endl<<endl;
	// placeholder for storing polynomial coefficient
	vector<double> coeff;
	polyfit(x_coord, y_coord, coeff, 3);
	cout << "Fitted coeff" << endl;
	for (int i = 0; i < coeff.size(); i++) {
		cout << coeff[i] << " ";
	}
	cout << endl << endl;
	vector<double> fitted_y_coord;
	cout << "Fitted y_coordinates" << endl;
	for (int p = 0; p < x_coord.size(); ++p)
	{
		double vfitted = coeff[0] + coeff[1] * x_coord.at(p) + coeff[2] * (pow(x_coord.at(p), 2)) + coeff[3] * (pow(x_coord.at(p), 3));
		cout << vfitted << " ";
		fitted_y_coord.push_back(vfitted);
	}
	cout << endl<<endl;
	// Adding noise
	// Define random generator with Gaussian distribution
	const double mean = 0.0;
	const double stddev = 2.0;
	default_random_engine generator;
	normal_distribution<double> dist(mean, stddev);

	// Add Gaussian noise
	for (auto& x : y_coord) {
		x = x + dist(generator);
	}
	cout << endl << "Noise added y_coordinates: " << endl<<endl;
	for (int i = 0; i < y_coord.size(); i++) {
		cout << y_coord[i] << " ";
	}
	cout << endl;
	//fitting the polynomial again
	vector<double> coeff1;
	polyfit(x_coord, y_coord, coeff1, 3);
	cout << "\nFitted coeff1" << endl;
	for (int i = 0; i < coeff1.size(); i++) {
		cout << coeff1[i] << " ";
	}
	cout << endl << endl;
	vector<double> fitted_y_coord1;
	cout<<"\nNew Fitted y_coordinates" << endl;
	for (int p = 0; p < x_coord.size(); ++p)
	{
		double vfitted1 = coeff1[0] + coeff1[1] * x_coord.at(p) + coeff1[2] * (pow(x_coord.at(p), 2)) + coeff1[3] * (pow(x_coord.at(p), 3));
		cout << vfitted1 << " ";
		fitted_y_coord1.push_back(vfitted1);
	}
	cout << endl << endl;
	return 0;
}