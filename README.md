# README #

Code for weekly sync call on 25th march

x coordinates : -10m to +10m

### Noise

Mean : 0, std dev: 2 m

# To Do for next sync call 

- Fit the polynomial in mm
- Take two ranges. R1 - -20 to 20 m, R2 - 0 to 600 m
- Add different sets of noises to x and y coordinates independently. 
- Fit all lane boundary borders simultaneously 